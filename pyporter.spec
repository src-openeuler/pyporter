Name:       pyporter
Version:    1.2
Release:    1
Summary:    A rpm packager bot for python modules from pypi.org
License:    Mulan PSL v2
URL:        https://gitee.com/openeuler/pyporter
Source0:    https://gitee.com/openeuler/pyporter/repository/archive/pyporter-v%{version}.tar.gz
BuildArch:  noarch
%description 
pyporter is a tool to create spec file and create rpm for python modules For more details, please use pyporter -h.

%package -n python3-pyporter
Summary:    A rpm packager bot for python modules from pypi.org

BuildRequires:  python3-hatchling python3-hatch-vcs python3-pip  python3-setuptools python3-wheel
Requires: wget python3-pip python3-retry2 openEuler-rpm-config >= 30-39 rpm-build

%description -n python3-pyporter
pyporter is a tool to create spec file and create rpm for python modules.

%prep
%autosetup -n pyporter-v%{version} -p1

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
pushd %{buildroot}

touch filelist.lst
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
popd
mv %{buildroot}/filelist.lst .

%files -n python3-pyporter -f filelist.lst
%{python3_sitelib}/*
%license LICENSE
%doc README.md

%changelog
* Wed Sep 27 2023 LukeLIN-web <1263810658@qq.com> - 1.2-1
- DESC: support packing pyproject.toml 

* Wed Aug 04 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.0-2
- DESC: delete BuildRequires gdb

* Wed Nov 4 2020 sunchendong<sunchendong@huawei.com> -1.0-1
- Type:NA
- ID:NA
- SUG:NA
- DESC:init package
